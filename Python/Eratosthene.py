def primes_until(n):
    """Return the list of prime numbers in 2..n (Got with sieve of Eratosthenes)"""
    candidate = [i for i in range(2, n+1)]
    prime = []
    while candidate != []:
        # First of current `candidate` is prime
        p = candidate[0]
        prime.append(p)
        # Sieve : candidate = candidate except p-multiple numbers
        candidate = [i for i in candidate if i % p != 0]
    return prime

if __name__ == "__main__":

    N = 10000

    p_list = primes_until(N)
    #Ugly but quick
    print(p_list)
    print(" => {:d} prime numbers from 2 to {:d}.".format(len(p_list), N))
