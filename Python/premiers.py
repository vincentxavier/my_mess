from math import sqrt

def premier(n):
    """Return True iif n is prime"""
    if n == 2: return True
    if n % 2 == 0: return False
    divis = 3
    r = sqrt(n)
    while divis <= r:
        if n % divis == 0: return False
        divis += 2
    return True


# But oublié :-( !
def prefixes_premiers():
    L = []
    # /!\  Add a list as element of a list 
    L.append([2, 3, 5, 7,])
    while True:
        r = []
        for k in L[-1]:
            for u in [1, 3, 5, 7, 9]:
                x = 10 * k + u
                if premier(u): r.append(x)
        if r == []: break
        L.append(r)
        print(L[-1])
        input()
    return L

print(prefixes_premiers())
