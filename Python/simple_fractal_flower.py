import turtle as TT

TT.speed(0)
# ===== DÉCLARATION DE LA FONCTION : ==============================================

def fleur(t):
    for i in range(8):
        carre(t)
        TT.left(45)

def carre(t):
    for i in range(9):
        TT.forward(t)
        TT.right(40)


tige = 200
TT.left(90)
TT.up()
TT.backward(300)
TT.down()
for i in range(5):
    TT.fd(tige)
    TT.left(30)
    TT.fd(tige*0.6)
    fleur(tige*0.05)
    TT.backward(tige*0.6)
    TT.rt(25)
    tige *= 0.7
TT.hideturtle()    
