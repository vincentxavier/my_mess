import turtle as TT
# Documentation, notamment sur
#   https://fr.wikibooks.org/wiki/Programmation_Python/Turtle
#   https://docs.python.org/3.4/library/turtle.html

# Vitesse du tracé : "slowest", "slow", "normal", "fast", "fastest"(=0), 1 à 10
TT.speed("fastest")    
TT.color("blue", "orange")     # (couleur des tracés, couleur pour «remplir»)

taille = 10            # longueur du segment tracé
ecart = 4              # "écart horizontal" entre segments
coeff = 1.1            # Par lequel multiplier la longueur des segments

print("largeur de fenêtre :", TT.window_width())
print("hauteur de fenêtre :", TT.window_height())

for i in range(25) :
    TT.forward(taille) # avance de taille pixels, idem TT.fd(taille)
    TT.up()            # lève le crayon (baissé au lancement de turtle)
                       # idem TT.penup() ou TT.pu()
    TT.back(taille)    # recule, idem TT.backward(taille) ou TT.bk(taille)
    TT.right(90)       # pivote à droite (sens horaire) de 90°, idem TT.rt(90)
    TT.forward(ecart)
    TT.left(90)        # pivote à gauche, idem TT.lt(90)
    TT.down()          # baisse le crayon, idem TT.pendown() ou TT.pd()
    taille = taille * coeff    # Qui peut être remplacé par :    taille *= coeff

TT.exitonclick()	   # Ferme la fenêtre sur un clic...
