#!/usr/bin/python3
import http.server
import socketserver

# Serveur http de base delivrant le contenu du repertoire courant via le port indique.
PORT = 5437
Handler = http.server.SimpleHTTPRequestHandler
httpd = socketserver.TCPServer(("",PORT), Handler)
print("à l'écoute sur le port :", PORT)

# sert la page index.html ou sinon liste le dossier ...
httpd.serve_forever()
