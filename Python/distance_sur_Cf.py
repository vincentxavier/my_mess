from math import *

def distance(x1, y1, x2, y2):
    return sqrt((x2-x1)**2+(y2-y1)**2)

def distance_pt(A, B):
    return distance(A[0], A[1], B[0], B[1])

def long_Cf(f, a, b, n):
    assert n>0 and (type(n) is int)
    assert callable(f)  # f is a function
    
    long = 0
    x,y = a,f(a)
    step = (b-a) / n
    for i in range(n):
        xx = x + step
        if xx>b :       # To avoid error caused by 
            xx=b        #  float binary representation
        yy = f(xx)
        long += distance(x,y,xx,yy)
        x,y = xx,yy
    return long

if __name__ == "__main__":
    d = long_Cf(lambda x:x*x, -1, 3, 100)
    print("Distance sur la courbe de x², de x=-1 à x=3 : {:.2f} env.".format(d))
