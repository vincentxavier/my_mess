from matplotlib import pyplot as plt

def suivant(n):
    """
    Renvoie le terme suivant dans la suite de Syracuse
    """
    # assert type(n) is int and n > 0
    return n // 2 if n % 2 == 0 else 3 * n + 1

def syracuse(n):
    """
    Itérateur sur la suite de Syracuse, partant de n
    """
    while 1:
        yield n
        n = suivant(n)

def hauteur_vol(n):
    """
    Renvoie la "hauteur de vol" de n dans la suite de Syracuse
    """
    h = n
    for i in syracuse(n):
        if i == 1 :
            return h
        if i > h:
            h = i
            
def hauteur_max_until(n):
    """
    Renvoie le couple (i,h) avec la hauteur max. h et le premier
    entier associé i, dans la suite de Syracuse, pour les entiers
    inférieurs ou égaux à n
    """
    h_max = n
    n_hmax = n
    for i in range(1, n+1):
        h = hauteur_vol(i)
        if h > h_max:
            h_max = h
            n_hmax = i
    return n_hmax, h_max

def graph_hauteur(n):
    """
    Représente les hauteurs de vol en fonction de l'entier, de 1 à n
    """
    x = [ i for i in range(1, n + 1) ]
    y = [hauteur_vol(i) for i in range(1, n + 1) ]
    plt.scatter(x, y, color='red')
    plt.title("Hauteurs de vol")
    plt.ylabel("Hauteur")
    plt.xlabel("n")
    plt.show()
    
def duree_vol(n):
    """
    Renvoie la "durée de vol" de n dans la suite de Syracuse
    """
    d = 0
    for i in syracuse(n):
        if i == 1 :
            return d
        d += 1
        
def duree_max_until(n):
    """
    Renvoie le couple (i,d) avec la durée max. d et le premier
    entier associé i, dans la suite de Syracuse, pour les entiers
    inférieurs ou égaux à n
    """
    d_max = 0
    n_dmax = n
    for i in range(1, n+1):
        d = duree_vol(i)
        if d > d_max:
            d_max = d
            n_dmax = i
    return n_dmax, d_max

def graph_duree(n):
    """
    Représente les durées de vol en fonction de l'entier, de 1 à n
    """
    x = [ i for i in range(1, n + 1) ]
    y = [duree_vol(i) for i in range(1, n + 1) ]
    plt.scatter(x, y, color='green', s=1)
    plt.title("Durées de vol")
    plt.ylabel("Durée")
    plt.xlabel("n")
    plt.show()

if __name__ == "__main__":
    N = 42
    print("Analyse graphique des hauteurs, puis durées de vol, pour", N, "...")
    print("(Si besoin, fermer les fenêtres pour passer à la suite)")
    graph_hauteur(N)
    graph_duree(N)
    print("[Fin d'exécution]")
