from math import sqrt

def fibo(n):
    """
    Provides n first Fibonacci numbers, from F_0 = 1 (not 0).
    """
    a, b = 1, 1   # or a = 0 (but 1 avoid issue in f_i/mem)
    yield a
    for i in range(n):
        yield b
        c = a + b
        a, b = b, c

if __name__ == "__main__":
    phi = (1 + sqrt(5))/2
    print("PHI \u2248 {:f}".format(phi))  # "\u2248" == "≈"

    mem = 1
    for i, f_i in enumerate(fibo(15)):
        q = f_i / mem
        print("F_{:d} = {:d} \t F_{:d} / F_{:d} ≈ {:f} \t ({:.3f} pour mille)"
              .format(i, f_i, i, i-1, q, 1000.0 * abs(q - phi) / phi))
        mem = f_i
    
    
